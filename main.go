package main

import (
  "fmt"
  "net/http"
  log "github.com/sirupsen/logrus"
)

func main() {
  http.Handle("/", loggingMiddleware(http.HandlerFunc(handler)))
  log.Infof("Listening on port 8089 for incoming HTTP requests")
  http.ListenAndServe(":8089", nil)
}

func handler(w http.ResponseWriter, r *http.Request) {
  fmt.Fprintf(w, "Hello modules world!\n")
}

func loggingMiddleware(next http.Handler) http.Handler {
  return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
      log.Infof("URI: %s", r.RequestURI)
      next.ServeHTTP(w, r)
  })
}
