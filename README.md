# hello-world-module

Hello world Go module.

## Steps
### Initialize modules
```
export GO111MODULE=on
go mod init
```

### Install some dependency
`go get github.com/sirupsen/logrus`

Check `go.mod` and `go.sum` files.

### Build
`go build`

### Run
`./hello-world-module`

### Test
```
$ curl localhost:8089
Hello modules world!
```

## Docker
### Build image
`docker build -t golang/hello-world-module:0.0.1 .`

### Run container
`docker run -it --rm --name hello-world-module --net=host golang/hello-world-module:0.0.1`