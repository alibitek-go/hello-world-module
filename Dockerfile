# Step 1: Build executable
FROM golang:1.12-alpine3.10 AS builder

ENV GO111MODULE=on

WORKDIR /app

COPY go.mod go.sum ./
COPY . .

RUN apk update && apk add --no-cache git \
    && adduser -D -g '' appuser \
    && go mod download \
    && go mod verify \
    && CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s"

# Step 2: Run in small image
FROM scratch

COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /app/hello-world-module /app/hello-world-module

USER appuser

EXPOSE 8089

ENTRYPOINT ["/app/hello-world-module"]
